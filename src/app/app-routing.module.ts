import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AgendaComponent} from './agenda/agenda.component';
import {ProfileComponent} from './profile/profile.component';

const appRoutes: Routes = [
  {path: '', redirectTo: 'agenda', pathMatch: 'full'},
  {path: 'agenda', component: AgendaComponent},
  {path: 'profile', component: ProfileComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
