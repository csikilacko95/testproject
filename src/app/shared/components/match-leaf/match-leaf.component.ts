import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Match} from '../../models/match.model';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Team} from '../../models/team.model';

@Component({
  selector: 'app-match-leaf',
  templateUrl: './match-leaf.component.html',
  styleUrls: ['./match-leaf.component.css']
})
export class MatchLeafComponent implements OnInit {
  @Input() match: Match;
  @Input() teams: Team[];
  @Input() freeEntity = false; // to decide if it's a free entity where we create new matches.
  @Output() update: EventEmitter<Match> = new EventEmitter<Match>();
  @Output() remove: EventEmitter<Match> = new EventEmitter<Match>();
  @Output() updatePlayerState: EventEmitter<Match> = new EventEmitter<Match>();
  totalPlayers = 0;
  editMode = false;
  checkedPlayer = 0;
  teamList: Team[];

  team1 = new FormControl('', [
      Validators.required
  ]);
  team2 = new FormControl('', [
    Validators.required
  ]);
  date = new FormControl('', [
    Validators.required
  ]);

  matchForm: FormGroup = this.builder.group({
    team1: this.team1,
    team2: this.team2,
    date: this.date
  });

  constructor(private builder: FormBuilder) {}

  ngOnInit() {
    this.teamList =  this.teams;

    // Get number of players
    if (!this.freeEntity) {
      this.match.teams.forEach(t => {
        this.totalPlayers += t.players.length;
        t.players.forEach(p => {
          if (p.check) {
            this.checkedPlayer++;
          }
        });
      });
    }
  }

  save() {
    const id = this.editMode ? this.match.id : 0;
    const team1 = this.getTeam(this.matchForm.value.team1);
    const team2 = this.getTeam(this.matchForm.value.team2);
    const match: Match = new Match(id, [team1, team2], this.matchForm.value.date);
    this.update.emit(match);
    this.matchForm.reset();
    this.editMode = false;
  }

  // Just for testing!
  togglePlayerState(teamid: number, playerid: number) {
    const teamIndex = this.match.teams.findIndex(s => s.id === teamid);
    const playerIndex = this.match.teams[teamIndex].players.findIndex(s => s.id === playerid);

    if (teamIndex !== -1 && playerIndex !== -1) {
      const player = this.match.teams[teamIndex].players[playerIndex];
      player.check = !player.check;
      this.updatePlayerState.emit(this.match);
    }

    this.checkedPlayer = 0;
    // Update number of checked players
    this.match.teams.forEach(t => {
      t.players.forEach(p => {
        if (p.check) {
          this.checkedPlayer++;
        }
      });
    });
  }

  getTeam(id: number): Team {
    return this.teamList.find(s => s.id === id);
  }

  // Note: this should be changed to pipe!
  dateToString(date: Date) {
    return date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
  }

  onEdit() {
    this.editMode = true;  // in this case we just want to show the free match-leaf entity
    this.matchForm.setValue({
      date: this.match.date,
      team1: this.match.teams[0].id,
      team2: this.match.teams[1].id
    });
  }

  onRemove() {
    this.remove.emit(this.match);

  }
}
