export class Player {
  constructor(public id: number,
              public name: string,
              public check: boolean) {}
}
