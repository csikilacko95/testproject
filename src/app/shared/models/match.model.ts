import {Team} from './team.model';

export class Match {
  constructor (public id: number,
               public teams: Team[],
               public date: Date) {}
}
