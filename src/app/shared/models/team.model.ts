import {Player} from './player.model';

export class Team {
  constructor(public id,
              public name: string,
              public players: Player[]) {}
}
