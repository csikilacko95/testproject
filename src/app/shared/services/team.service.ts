import { Injectable } from '@angular/core';
import {Team} from '../models/team.model';
import {Player} from '../models/player.model';

@Injectable()
export class TeamService {
  public _teamList: Team[];

  constructor() {
    this.loadTeamList();
  }

  // For further uses
  public loadTeamList() {
    // load with dummy data

    // Dummy Players
    const player1 = new Player(1, 'John Doe', false);
    const player2 = new Player(2, 'King Kong', false);
    const player3 = new Player(3, 'Smith John', false);
    const player4 = new Player(4, 'Jack the Ripper', false);
    const player5 = new Player(5, 'Alan Walker', false);
    const player6 = new Player(6, 'Alma Ecet', false);
    const player7 = new Player(7, 'Bor Ecet', false);
    const player8 = new Player(8, 'King Carl', false);
    const player9 = new Player(9, 'Alex Heinrich', false);
    const player10 = new Player(10, 'Robert', false);
    const player11 = new Player(11, 'Alex', false);
    const player12 = new Player(12, 'God', false);

    // Dummy Teams;
    const Team1 = new Team(1, 'Horus', [player1, player2, player3]);
    const Team2 = new Team(2, 'Winners', [player4, player5, player6]);
    const Team3 = new Team(3, 'Losers', [player7, player8, player9]);
    const Team4 = new Team(4, 'Castigatorii', [player10, player11, player12]);

    this._teamList = [Team1, Team2, Team3, Team4];
  }

  // Get index
  private getIndex(id: number) {
    return this._teamList.findIndex(s => s.id === id);
  }
}
