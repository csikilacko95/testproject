import { Injectable } from '@angular/core';
import {Match} from '../models/match.model';
import {Subject} from 'rxjs/Subject';
import {Player} from '../models/player.model';
import {Team} from '../models/team.model';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {TeamService} from './team.service';

@Injectable()
export class MatchService {
  private _matchList: Match[];
  public matchListObs = new BehaviorSubject<Match[]>(this._matchList);
  teamList: Team[];

  constructor(teamService: TeamService) {
    this.teamList = teamService._teamList;
      this.loadMatchList();
  }

  // For further uses
  public loadMatchList() {
    // load with dummy data
    this._matchList = [
      new Match(1, [this.teamList[0], this.teamList[1]], new Date(2018, 2, 12)),
      new Match(2, [this.teamList[2], this.teamList[3]], new Date(2018, 3, 30)),
      new Match(3, [this.teamList[0], this.teamList[3]], new Date(2018, 4, 18))
    ];
    this.matchListObs.next(this._matchList);
  }

  // New match inserting into our matchList
  public insertMatch(match: Match) {
    // Change dummy match id
    if (match.id === 0) {
      match.id = this.getLastId() + 1;
    }

    this._matchList.push(match);
    this.matchListObs.next(this._matchList);
  }

  // Get match from matchList by id???
  public getMatch(id: number) {
    return this._matchList.find(
      s => s.id === id
    );
  }

  // Remove match from matchList by id
  public removeMatch(id: number) {
    const index = this.getIndex(id);
    if (index !== -1) {
      this._matchList.splice(index, 1);
      this.matchListObs.next(this._matchList);
    }
  }

  // Update a match
  public updateMatch(match: Match) {
    if (match.id === 0) {
      // it's a new match, so we save it into our matchlist
      this.insertMatch(match);
    } else {
      // it's an existing match, so we update it
      const index = this.getIndex(match.id);
      if (index !== -1) {
        this._matchList[index] = match;
        this.matchListObs.next(this._matchList);
      }
    }
  }

  // Get index
  private getIndex(id: number) {
    return this._matchList.findIndex(s => s.id === id);
  }

  public getMatchListObs(): Observable<Match[]> {
    return this.matchListObs.asObservable();
  }

  // To get the last match id from matchList
  public getLastId() {
    if (this._matchList.length > 0) {
      return Math.max.apply(Math, this._matchList.map((obj) => obj.id));
    }
    return 0;
  }
}
