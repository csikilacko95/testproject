import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';

import {CdkTableModule} from '@angular/cdk/table';
import {
  MatAutocompleteModule, MatButtonModule, MatButtonToggleModule, MatCardModule,
  MatChipsModule, MatStepperModule
} from '@angular/material';
import {MatDatepickerModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatDialogModule, MatExpansionModule} from '@angular/material';
import {MatGridListModule} from '@angular/material';
import {MatIconModule} from '@angular/material';
import {MatListModule} from '@angular/material';
import {MatPaginatorModule} from '@angular/material';
import {MatNativeDateModule} from '@angular/material';
import {MatInputModule, MatMenuModule} from '@angular/material';
import {MatProgressBarModule} from '@angular/material';
import {MatProgressSpinnerModule} from '@angular/material';
import {MatSidenavModule} from '@angular/material';
import {MatSliderModule} from '@angular/material';
import {MatSelectModule} from '@angular/material';
import {MatRippleModule} from '@angular/material';
import {MatRadioModule} from '@angular/material';
import {MatSnackBarModule} from '@angular/material';
import {MatTabsModule} from '@angular/material';
import {MatToolbarModule} from '@angular/material';
import {MatTooltipModule} from '@angular/material';
import {MatTableModule} from '@angular/material';
import {MatSortModule} from '@angular/material';
import {MatSlideToggleModule} from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ProfileComponent } from './profile/profile.component';
import {AppRoutingModule} from './app-routing.module';
import {MatchService} from "./shared/services/match.service";
import { MatchLeafComponent } from './shared/components/match-leaf/match-leaf.component';
import {AngularDateTimePickerModule} from "angular2-datetimepicker";
import {TeamService} from "./shared/services/team.service";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AgendaComponent,
    ProfileComponent,
    MatchLeafComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    NoopAnimationsModule,
    FlexLayoutModule,
    NoopAnimationsModule,
    CdkTableModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    AngularDateTimePickerModule
  ],
  providers: [MatchService, TeamService],
  bootstrap: [AppComponent]
})
export class AppModule { }
