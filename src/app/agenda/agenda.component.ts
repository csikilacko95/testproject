import { Component, OnInit } from '@angular/core';
import {MatchService} from '../shared/services/match.service';
import {Match} from '../shared/models/match.model';
import {Team} from '../shared/models/team.model';
import {Observable} from 'rxjs/Observable';
import {TeamService} from '../shared/services/team.service';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})
export class AgendaComponent implements OnInit {
  matchList$: Observable<Match[]>;
  teamList$: Team[];
  showFreeEntity = false;

  constructor(private matchService: MatchService, private teamService: TeamService) {}

  ngOnInit() {
    this.matchList$ = this.matchService.getMatchListObs();
    this.teamList$ = this.teamService._teamList;
  }

  newMatch() {
    this.showFreeEntity = true;
  }

  onMatchUpdate(match: Match) {
    this.matchService.updateMatch(match);
    this.showFreeEntity = false;
  }

  onMatchRemove(match: Match) {
    this.matchService.removeMatch(match.id);
  }

  onUpdatePlayerState(match: Match) {
    this.matchService.updateMatch(match);
    }

}
